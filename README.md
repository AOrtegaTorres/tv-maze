This project connect tv-maze API

## Scripts
to run the project execute
- yarn (root)
- yarn start (to start )

## Dependencies
This project uses:
  - React
  - Redux
  - React Router
  - React Pagination
  - JSS
  - Eslint (airbnb style guide)
