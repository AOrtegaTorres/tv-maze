module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'plugin:react/recommended',
    'airbnb'
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: [
    'react',
    'only-warn'
  ],
  ignorePatterns: ['serviceWorker.js'],
  rules: {
    'react/jsx-filename-extension': [0, { extensions: ['.js', '.jsx'] }],
    'react/function-component-definition': ['error', {
      namedComponents: 'arrow-function'
    }],
    'react/jsx-props-no-spreading': [0, {
      custom: 'ignore'
    }],
    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: [
        'state'
      ]
    }],
    'no-unused-vars': [1, { vars: 'local', args: 'after-used', ignoreRestSiblings: false }],
    'linebreak-style': 0,
    'no-console': 0,
    'comma-dangle': ['error', 'never'],
    'import/prefer-default-export': 0,
    'prefer-promise-reject-errors': 0
  }
};
