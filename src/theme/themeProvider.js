import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'react-jss';

const StyleProvider = ({ children }) => {
  const [screenSize, setScreenSize] = useState('');
  const theme = {
    screenSize
  };

  const handleScreenChange = () => {
    const { documentElement } = document;
    const { clientWidth } = documentElement;
    if (clientWidth > 320 && clientWidth < 480) setScreenSize('small');
    if (clientWidth >= 480 && clientWidth < 1024) setScreenSize('medium');
    if (clientWidth >= 1024 && clientWidth < 1700) setScreenSize('large');
    if (clientWidth >= 1700 && clientWidth < 2000) setScreenSize('xlarge');
  };

  useEffect(() => {
    handleScreenChange();
  }, [document.documentElement.clientWidth]);

  window.addEventListener('resize', handleScreenChange);

  return (<ThemeProvider theme={theme}>{children}</ThemeProvider>);
};

StyleProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export default StyleProvider;
