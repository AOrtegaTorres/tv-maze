import Home from './Home';
import App from './App';
import Show from './Show';

export default {
  Home,
  App,
  Show
};
