import { safeGet } from '../../../utils';

const vars = {
  header: {
    width_small: '100%',
    width_medium: '100%',
    width_large: '60%',
    width_xlarge: '60%'
  }
};

export default ({ screenSize }) => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  content: {
    width: '60%',
    display: 'flex',
    justifyContent: 'space-around',
    flexFlow: 'row wrap'
  },
  left: {
    display: 'flex',
    flexFlow: 'row nowrap',
    padding: '20px 0px',
    justifyContent: 'space-between',
    '& img': {
      width: 600
    }
  },
  right: {
    width: '40%',
    display: 'flex',
    flexDirection: 'column'
  }
});
