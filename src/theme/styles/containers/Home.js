import { safeGet } from '../../../utils';

const vars = {
  header: {
    width_small: '100%',
    width_medium: '100%',
    width_large: '60%',
    width_xlarge: '60%',
    height_small: 50,
    height_medium: 80,
    height_large: 120,
    height_xlarge: 120
  }
};

export default ({ screenSize }) => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    width: safeGet(vars, `header.width_${screenSize}`, '60%'),
    height: safeGet(vars, `header.height_${screenSize}`, 80),
    display: 'flex',
    flexFlow: 'row nowrap',
    padding: '20px 0px',
    justifyContent: 'space-between'
  },
  content: {
    width: '60%',
    display: 'flex',
    justifyContent: 'flex-start',
    flexFlow: 'row wrap'
  },
  row: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between'
  },
  containerPagination: {
    display: 'flex',
    paddingLeft: 0,
    listStyle: 'none',
    border: '1px solid #00B4D8'
  },
  pageItem: {
    color: '#0077B6',
    height: 40,
    width: 'auto',
    padding: 10,
    boxSizing: 'border-box',
    cursor: 'pointer'
  },
  previousLink: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    padding: '0 10px'
  },
  pageLink: {
    height: 40,
    width: 'auto',
    padding: 10,
    boxSizing: 'border-box',
    cursor: 'pointer'
  },
  active: {
    color: 'white',
    backgroundColor: '#0077B6'
  }
});
