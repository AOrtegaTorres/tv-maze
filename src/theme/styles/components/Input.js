export default {
  root: {
    width: 400,
    height: 40,
    border: 'none',
    display: 'flex',
    borderRadius: 5,
    flexFlow: 'row nowrap'
  },
  input: {
    boxShadow: '5px 8px 17px -6px rgba(0,0,0,0.75) inset;',
    border: 'none',
    width: 350,
    borderRadius: '10px 0px 0px 10px',
    fontSize: 20,
    padding: '0px 10px',
    color: '#6d6875'
  },
  iconWrapper: {
    width: 50,
    height: 40,
    backgroundColor: '#0077B6',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer'
  },
  icon: {
    fontSize: 20,
    color: 'white'
  }
};
