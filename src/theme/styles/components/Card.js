export default {
  root: {
    width: 210,
    height: 350,
    margin: 10,
    boxShadow: '2px 5px 10px #8f9495',
    display: 'flex',
    color: 'white',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  cardHeader: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    cursor: 'pointer',
    '&:img': {
      width: 210
    }
  },
  title: {
    maxWidth: 210,
    padding: 20,
    fontSize: 20,
    textAlign: 'center',
    height: '100%',
    backgroundColor: '#0077B6',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden'
  },
  rating: {
    fontSize: 18,
    width: 60,
    height: 40,
    marginLeft: 150,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 1,
    backgroundColor: '#0077B6'
  },
  '@keyframes FadeIn': {
    '0%': {
      opacity: 0
    },
    '50%': {
      opacity: 0,
      height: 60
    },
    '100%': {
      opacity: 1,
      height: 80
    }
  },
  info: {
    width: 210,
    height: 80,
    fontSize: 18,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    position: 'absolute',
    animationName: '$FadeIn',
    animationDuration: '.5s',
    marginBottom: -270,
    backgroundColor: '#0096C7'
  }
};
