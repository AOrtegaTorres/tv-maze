export default {
  root: {
    width: 500,
    height: 60,
    backgroundColor: '#0077B6',
    display: 'flex',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    fontSize: 20,
    cursor: 'pointer'
  }
};
