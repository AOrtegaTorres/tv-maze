import React from 'react';
import withStyles from 'react-jss';
import styles from './styles';

export const themedHOC = (path, Component) => {
  const pathArray = path.split('.');
  const styledApp = withStyles(styles[pathArray[0]][pathArray[1]])(
    (props) => <Component {...props} />
  );

  return styledApp;
};
