import { configureStore } from '@reduxjs/toolkit';
import home from '../containers/Home/slice';
import show from '../containers/Show/slice';

export const store = configureStore({
  reducer: {
    home,
    show
  }
});
