import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './containers/Home';
import Show from './containers/Show/Show';
import StyleProvider from './theme/themeProvider';

const App = () => (
  <StyleProvider>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/show/:id" element={<Show />} />
      </Routes>
    </BrowserRouter>
  </StyleProvider>
);

export default App;
