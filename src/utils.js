export const safeGet = (obj, path, defaultValue = null) => {
  if (!obj || !path) return defaultValue;

  try {
    return String.prototype.split.call(path, /[,[\].]+?/)
      .filter(Boolean)
      .reduce((a, c) => (Object.hasOwnProperty.call(a, c) ? a[c] : defaultValue), obj);
  } catch (e) {
    return defaultValue;
  }
};

export const getDuration = (minutes) => {
  const hours = Math.trunc(minutes / 60);
  const minutesRemaining = minutes - (hours * 60);
  return `${hours}.${minutesRemaining} hour(s)`;
};

export const getFormatedSchedule = (time, language) => {
  if (language === 'English') return time;
  if (time !== '') {
    const hoursAndMinutes = time.split(':');
    let hours = Number(hoursAndMinutes[0]);
    let minutes = hoursAndMinutes[1];
    if (minutes === '0') {
      minutes = '00';
    }
    const amOrPm = hours >= 12 ? 'pm' : 'am';
    hours = (hours % 12) || 12;
    return `${hours}:${minutes} ${amOrPm}`;
  }
  return 'N/A';
};
