import { createSlice } from '@reduxjs/toolkit';

export const homeSlice = createSlice({
  name: 'home',
  initialState: {
    show: '',
    showList: [],
    ui: {
      itemPerPage: 5,
      pageCount: 0,
      itemOffset: 0,
      currentRows: []
    }
  },
  reducers: {
    setShow: (state, action) => {
      const { payload } = action;
      state.show = payload;
    },
    setShowList: (state, action) => {
      const { payload } = action;
      state.showList = payload;
    },
    setPageCount: (state, action) => {
      const { payload } = action;
      state.ui.pageCount = payload;
    },
    setItemOffset: (state, action) => {
      const { payload } = action;
      state.ui.itemOffset = payload;
    },
    setCurrentRows: (state, action) => {
      const { payload } = action;
      state.ui.currentRows = payload;
    }
  }
});

export default homeSlice.reducer;
