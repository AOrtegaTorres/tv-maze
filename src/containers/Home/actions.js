import { homeSlice } from './slice';

export const {
  setShow,
  setShowList,
  setItemOffset,
  setCurrentRows,
  setPageCount
} = homeSlice.actions;
