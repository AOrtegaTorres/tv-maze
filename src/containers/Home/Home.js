import React, { useEffect, memo, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import ReactPaginate from 'react-paginate';
import { useNavigate } from 'react-router-dom';
import {
  selectShow,
  selectShowList,
  selectUi
} from './selectors';
import {
  setShowList,
  setItemOffset,
  setPageCount,
  setCurrentRows,
  setShow
} from './actions';
import { themedHOC } from '../../theme/themedHOC';
import Card from '../../components/Card';
import Input from '../../components/Input';

const Home = (props) => {
  const { classes } = props;
  const navigate = useNavigate();
  const showList = useSelector(selectShowList);
  const show = useSelector(selectShow);
  const ui = useSelector(selectUi);
  const dispatch = useDispatch();

  const paginateRows = () => {
    const { itemOffset, itemPerPage } = ui;
    const endOffset = itemOffset + itemPerPage;
    const currentRows = showList.slice(itemOffset, endOffset);
    const getPageCount = Math.ceil(showList.length / itemPerPage);

    dispatch(setCurrentRows(currentRows));
    dispatch(setPageCount(getPageCount));
  };

  const handleTotalRows = (rowsAry) => {
    const rowsSpace = [...Array(Math.ceil(rowsAry.length / 5))];
    const rows = rowsSpace.map((row, index) => rowsAry.slice(index * 5, index * 5 + 5));
    dispatch(setShowList(rows));
  };

  useEffect(() => {
    const fetchShows = async () => {
      const requestData = await fetch('http://api.tvmaze.com/shows');
      const data = await requestData.json();
      handleTotalRows(data);
    };
    fetchShows();
  }, []);

  useEffect(() => {
    if (showList.length > 0) paginateRows();
  }, [showList, ui.itemOffset]);

  const handleClick = (id) => {
    navigate(`/show/${id}`);
  };

  const createRows = () => useMemo(() => {
    const rows = ui.currentRows.map((row, index) => (
      <div key={index} className={classes.row}>
        {
          row.map((showItem) => <Card key={showItem.id} {...showItem} onClick={handleClick} />)
        }
      </div>
    ));
    return rows;
  }, [ui.currentRows]);

  const handlePageClick = (e) => {
    const newOffset = (e.selected * ui.itemPerPage) % showList.length;
    dispatch(setItemOffset(newOffset));
  };

  const handleChange = (e) => {
    dispatch(setShow(e.target.value));
  };

  const fetchQuery = async () => {
    const requestData = await fetch(`http://api.tvmaze.com/search/shows?q=${show}`);
    const data = await requestData.json();
    if (data.length > 0) {
      const newData = await data.map((showItem) => showItem.show);
      handleTotalRows(newData);
    } else {
      // eslint-disable-next-line no-alert
      alert('No results found');
    }
  };

  const handleSubmit = () => {
    fetchQuery();
  };

  return (
    <div className={classes.root}>
      <header className={classes.header}>
        <Input onChange={handleChange} onSubmit={handleSubmit} />
      </header>
      <div className={classes.content}>
        {
          createRows()
        }
        <ReactPaginate
          nextLabel="Next >"
          breakLabel="..."
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          pageCount={ui.pageCount}
          activeClassName={classes.active}
          pageClassName={classes.pageItem}
          pageLinkClassName={classes.pageLink}
          breakLinkClassName={classes.pageLink}
          previousLinkClassName={classes.previousLink}
          nextLinkClassName={classes.previousLink}
          containerClassName={classes.containerPagination}
          previousLabel="< Previous"
          renderOnZeroPageCount={null}
        />
      </div>
    </div>
  );
};

Home.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string)
};

Home.defaultProps = {
  classes: {}
};

export default memo(themedHOC('containers.Home', Home));
