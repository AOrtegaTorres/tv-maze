export const selectShowList = (state) => state.home.showList;
export const selectShow = (state) => state.home.show;
export const selectUi = (state) => state.home.ui;
