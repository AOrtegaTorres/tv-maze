import { showSlice } from './slice';

export const {
  setShow
} = showSlice.actions;
