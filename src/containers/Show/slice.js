import { createSlice } from '@reduxjs/toolkit';

export const showSlice = createSlice({
  name: 'show',
  initialState: {
    show: {}
  },
  reducers: {
    setShow: (state, action) => {
      const { payload } = action;
      state.show = payload;
    }
  }
});

export default showSlice.reducer;
