import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setShow } from './actions';
import { selectShow } from './selectors';
import { themedHOC } from '../../theme/themedHOC';
import { getDuration } from '../../utils';
import Button from '../../components/Button';

const Show = (props) => {
  const { classes } = props;
  const params = useParams();
  const dispatch = useDispatch();
  const show = useSelector(selectShow);

  useEffect(() => {
    const { id } = params;
    const fetchShow = async () => {
      const requestData = await fetch(`http://api.tvmaze.com/shows/${id}`);
      const data = await requestData.json();
      dispatch(setShow(data));
    };
    fetchShow();
  }, []);

  const handleClick = () => {
    window.location = show.url;
  };

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <div className={classes.left}>
          <img src={show.image?.original} alt={show.name} />
        </div>
        <div className={classes.right}>
          <h1>
            {
              `${show.name} (${show.status})`
            }
          </h1>
          <h3>{`Duration - ${getDuration(show.averageRuntime)}`}</h3>
          <h3>Genres:</h3>
          <ul>
            {
              show.genres?.map((genre) => (
                <li key={genre}>{genre}</li>
              ))
            }
          </ul>
          <div dangerouslySetInnerHTML={{ __html: show.summary }} />
          <Button text="Go!" onClick={handleClick} />
        </div>
      </div>
    </div>
  );
};

Show.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
  show: PropTypes.shape({
    name: PropTypes.string,
    averageRuntime: PropTypes.number,
    schedule: PropTypes.shape({
      time: PropTypes.string
    }),
    language: PropTypes.string
  })
};

Show.defaultProps = {
  classes: {},
  show: {
    name: '',
    averageRuntime: '',
    schedule: {
      time: ''
    },
    language: ''
  }
};

export default themedHOC('containers.Show', Show);
