import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { themedHOC } from '../../theme/themedHOC';

const Input = (props) => {
  const {
    classes,
    onChange,
    onSubmit
  } = props;

  const handleChange = (e) => {
    if (onChange) onChange(e);
  };

  const handleClick = () => {
    if (onSubmit) onSubmit();
  };

  const handleSubmit = (e) => {
    const key = e.which || e.keyCode;
    if (key === 13) onSubmit();
  };

  return (
    <div className={classes.root}>
      <input className={classes.input} onChange={handleChange} onKeyPress={handleSubmit} />
      <div className={classes.iconWrapper} onClick={handleClick}>
        <FontAwesomeIcon icon={faSearch} className={classes.icon} />
      </div>
    </div>
  );
};

Input.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
  onChange: PropTypes.func,
  onSubmit: PropTypes.func
};

Input.defaultProps = {
  classes: {},
  onChange: undefined,
  onSubmit: undefined
};

export default themedHOC('components.Input', Input);
