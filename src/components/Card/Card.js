/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, memo } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { themedHOC } from '../../theme/themedHOC';
import { getDuration, getFormatedSchedule } from '../../utils';

const Card = (props) => {
  const [info, setInfo] = useState(false);
  const {
    classes,
    onClick,
    image,
    name,
    language,
    averageRuntime,
    schedule: {
      time
    },
    rating: { average },
    id
  } = props;

  const handleMouseOver = () => {
    setInfo(true);
  };

  const handleMouseLeave = () => {
    setInfo(false);
  };

  const handleClick = () => {
    if (onClick) onClick(id);
  };

  return (
    <div
      className={classes.root}
      onMouseOver={handleMouseOver}
      onMouseLeave={handleMouseLeave}
      onClick={handleClick}
    >
      <div className={classes.cardHeader}>
        <div className={classes.rating}>
          <FontAwesomeIcon icon={faStar} />
          <h5>{average}</h5>
        </div>
        <img src={image ? image.medium : `${process.env.PUBLIC_URL}/no-image.jpg`} alt={name} />
      </div>
      <div className={classes.title}>
        {name}
      </div>
      {
        info && (
          <div className={classes.info}>
            <div>{`Duration: ${getDuration(averageRuntime)}`}</div>
            <div>{`Schedule: ${getFormatedSchedule(time, language)}`}</div>
          </div>
        )
      }
    </div>
  );
};

Card.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
  image: PropTypes.shape({
    medium: PropTypes.string
  }),
  name: PropTypes.string,
  averageRuntime: PropTypes.number,
  rating: PropTypes.shape({
    average: PropTypes.number
  }),
  schedule: PropTypes.shape({
    time: PropTypes.string,
    days: PropTypes.arrayOf(PropTypes.string)
  }),
  language: PropTypes.string,
  onClick: PropTypes.func,
  id: PropTypes.number
};

Card.defaultProps = {
  classes: {},
  image: {
    medium: ''
  },
  name: '',
  language: '',
  averageRuntime: 0,
  rating: {
    average: 0
  },
  schedule: {
    time: '',
    days: []
  },
  id: null,
  onClick: undefined
};

export default memo(themedHOC('components.Card', Card));
