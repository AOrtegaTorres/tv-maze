import React from 'react';
import PropTypes from 'prop-types';
import { themedHOC } from '../../theme/themedHOC';

const Button = (props) => {
  const {
    classes,
    text,
    onClick
  } = props;

  const handleClick = () => {
    if (onClick) onClick();
  };

  return (
    <div className={classes.root} onClick={handleClick}>
      {text}
    </div>
  );
};

Button.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
  text: PropTypes.string,
  onClick: PropTypes.func
};

Button.defaultProps = {
  classes: {},
  text: 'Button',
  onClick: undefined
};

export default themedHOC('components.Button', Button);
